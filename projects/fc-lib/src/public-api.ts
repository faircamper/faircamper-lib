/*
 * Public API Surface of fc-lib
 */

export * from './lib/fc-lib.service';
export * from './lib/fc-lib.component';
export * from './lib/fc-lib.module';

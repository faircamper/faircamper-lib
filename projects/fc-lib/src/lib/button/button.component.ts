import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'fc-lib-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent implements OnInit {
  @Input() color = 'basic';
  @Input() fill = false;
  @Input() disabled = false;
  constructor() {}

  ngOnInit(): void {}
}

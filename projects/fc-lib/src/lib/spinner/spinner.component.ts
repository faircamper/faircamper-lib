import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css'],
})
export class SpinnerComponent {
  @Input() color: 'black' | 'white' | 'gray' | 'slate' = 'black';
  @Input() showText = true;
  constructor() {}
}

import { CdkAccordion, CdkAccordionItem } from '@angular/cdk/accordion';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'fc-lib-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.css'],
})
export class ExpansionPanelComponent
  extends CdkAccordionItem
  implements OnInit, AfterViewInit
{
  @Input() rounded = false;
  @Input() insideAccordion = false;
  @Input() tabindex = 0;
  @ViewChild('content')
  content!: ElementRef;
  @ViewChild('innerContent')
  innerContent!: ElementRef;
  height = 0;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super(
      new CdkAccordion(),
      changeDetectorRef,
      new UniqueSelectionDispatcher()
    );
  }
  ngOnInit(): void {
    this.expandedChange.subscribe(() => {
      const element = this.content.nativeElement as HTMLElement;
      const scrollHeight = element.scrollHeight;
      if (!this.expanded) {
        this.height = 0;
      } else {
        this.height = scrollHeight;
      }
    });
  }

  ngAfterViewInit(): void {
    const element = this.content.nativeElement as HTMLElement;
    if (!this.expanded) {
      this.height = 0;
    } else {
      this.height = element.scrollHeight;
    }

    let eventHandler = (entries: ResizeObserverEntry[]) => {
      const contentH = entries[0].contentRect.height;
      if (this.height < contentH && this.expanded) {
        this.height = contentH;
        this.changeDetectorRef.detectChanges();
      }
      if (this.height > contentH && this.expanded) {
        this.height = contentH;
        this.changeDetectorRef.detectChanges();
      }
    };
    eventHandler = eventHandler.bind(this);
    const observer = new ResizeObserver(eventHandler);
    observer.observe(this.innerContent.nativeElement);
  }

  resize(px: number) {
    this.height = px;
  }
}

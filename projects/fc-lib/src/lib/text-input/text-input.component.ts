import {
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  FormControlStatus,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TextInputComponent,
    },
  ],
})
export class TextInputComponent implements ControlValueAccessor {
  @Input() placeholder = 'Placeholder';
  @Input() public formControlName: string | null = '';
  @Input() value = '';
  @Input() color: 'orange-500' | 'standard' = 'standard';
  @Output() change = new EventEmitter();
  @Output() keyUp = new EventEmitter();

  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null {
    if (!this.formControlName) {
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if (!this.control) {
      return 'PENDING';
    }
    return this.control.status;
  }

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  writeValue(obj: string): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = (e.target as any).value;
    this.onChangeFN(this.value);
  }

  onKeyUp(e: Event) {
    this.callOnChange(e);
    this.keyUp.emit(e);
  }
}

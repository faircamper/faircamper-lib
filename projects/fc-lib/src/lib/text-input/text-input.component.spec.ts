import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputErrorComponent } from '../input-error/input-error.component';
import { InputHintComponent } from '../input-hint/input-hint.component';
import { LabelComponent } from '../label/label.component';

import { TextInputComponent } from './text-input.component';

describe('TextInputComponent', () => {
  let component: TextInputComponent;
  let fixture: ComponentFixture<TextInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextInputComponent , LabelComponent, InputHintComponent, InputErrorComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

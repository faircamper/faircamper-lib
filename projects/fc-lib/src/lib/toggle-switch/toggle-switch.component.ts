import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlValueAccessor,
  ControlContainer,
  FormGroup,
  FormControl,
  FormControlStatus,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ToggleSwitchComponent),
    },
  ],
})
export class ToggleSwitchComponent implements ControlValueAccessor, OnInit {
  @Input() checked = false;
  @Input() placeholder = 'Placeholder';
  @Input() public formControlName: string | null = '';
  @Input() value: any;
  @Output() change = new EventEmitter();
  randomId: number = 0;

  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}
  ngOnInit(): void {
    this.randomId = Math.random();
  }

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null{
    if (!this.formControlName) {
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if (!this.control) {
      return 'PENDING';
    }
    return this.control.status;
  }

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  writeValue(obj: string): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = (e.target as any).checked;
    this.onChangeFN(this.value);
  }
}

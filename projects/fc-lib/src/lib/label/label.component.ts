import { Component, Input, OnInit } from '@angular/core';
import { FormControlStatus } from '@angular/forms';

@Component({
  selector: 'fc-lib-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.css'],
})
export class LabelComponent implements OnInit {
  @Input() status: FormControlStatus = 'PENDING';
  @Input() touched = false;
  constructor() {}

  ngOnInit(): void {}
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputErrorComponent } from '../input-error/input-error.component';
import { InputHintComponent } from '../input-hint/input-hint.component';
import { LabelComponent } from '../label/label.component';

import { TextAreaComponent } from './text-area.component';

describe('TextAreaComponent', () => {
  let component: TextAreaComponent;
  let fixture: ComponentFixture<TextAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextAreaComponent , LabelComponent, InputHintComponent, InputErrorComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

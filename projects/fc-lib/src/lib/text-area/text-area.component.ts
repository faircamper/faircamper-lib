import {
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  FormControlStatus,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TextAreaComponent,
    },
  ],
})
export class TextAreaComponent implements ControlValueAccessor {
  @Input() placeholder = 'Placeholder';
  @Input() rows = 3;
  @Input() public formControlName: string | null = '';
  @Input() value = '';
  @Output() change = new EventEmitter();
  @Output() keyUp = new EventEmitter();

  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null {
    if (!this.formControlName) {
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if (!this.control) {
      return 'PENDING';
    }
    return this.control.status;
  }

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  writeValue(obj: string): void {
    this.value = obj;
  }
  registerOnChange(fn: () => void): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: () => void): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = (e.target as any).value;
    this.onChangeFN(this.value);
  }

  onKeyUp(e: Event) {
    this.callOnChange(e);
    this.keyUp.emit(e);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputErrorComponent } from '../input-error/input-error.component';
import { InputHintComponent } from '../input-hint/input-hint.component';
import { LabelComponent } from '../label/label.component';

import { DatepickerComponent } from './datepicker.component';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatepickerComponent , LabelComponent, InputHintComponent, InputErrorComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set min and max date', () => {
    component.min = '2021-01-01';
    component.max = '2022-12-31';
    component.value = '2022-04-23'
    fixture.detectChanges();
    const newDatePicker = fixture.debugElement.nativeElement as HTMLElement
    expect(newDatePicker.querySelector('input')?.value).not.toEqual('');
    expect(new Date(component.max).getTime()).toBeGreaterThan(new Date(component.min).getTime());
  });

  it('should set disabled', () => {
    component.disabled = true;
    fixture.detectChanges();
    const newElement = fixture.nativeElement as HTMLElement
    expect(newElement.querySelector('input')?.disabled).toBeTruthy();
  });
  
  it('should set disabled', () => {
    component.disabled = true;
    fixture.detectChanges();
    const newElement = fixture.nativeElement as HTMLElement
    expect(newElement.querySelector('input')?.disabled).toBeTruthy();
  });

  it('should emit on change', () => {
    const spyChange = spyOn(component.change, 'emit')
    const testDate = '2022-01-23';
    component.change.emit(testDate)
    fixture.detectChanges();
    expect(spyChange).toHaveBeenCalledWith(testDate);
  });

  it('should emit on keyUp', () => {
    const spyKeyUp = spyOn(component.keyUp, 'emit')
    const testDate = '2021-01-23';
    component.keyUp.emit(testDate)
    fixture.detectChanges();
    expect(spyKeyUp).toHaveBeenCalledWith(testDate);
  });

  it('should emit on change', () => {
    const spyChange = spyOn(component.keyUp, 'emit')
    const testDate = '2022-01-23';
    component.keyUp.emit(testDate)
    fixture.detectChanges();
    expect(spyChange).toHaveBeenCalledWith(testDate);
  });
});

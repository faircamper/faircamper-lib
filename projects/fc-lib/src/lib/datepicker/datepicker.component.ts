import { formatDate } from '@angular/common';
import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  ControlContainer,
  FormGroup,
  FormControl,
  FormControlStatus,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true,
    },
  ],
})
export class DatepickerComponent implements ControlValueAccessor {
  @Input() public formControlName: string | null = '';
  @Input() disabled = false;

  @Input() dateFormatISO8601 = 'yyyy-MM-dd';
  @Input() dateLocale: 'de_DE' | 'en_EN' = 'en_EN';

  @Input() value: string = '';
  @Input() min: string = '';
  @Input() max: string = '';

  @Output() change = new EventEmitter();
  @Output() keyUp = new EventEmitter();

  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null {
    if (!this.formControlName) {
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if (!this.control) {
      return 'PENDING';
    }
    return this.control.status;
  }

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  writeValue(val: any): void {
    if (val instanceof Date) {
      this.value = formatDate(val, this.dateFormatISO8601, this.dateLocale);
    } else {
      this.value = formatDate(
        val !== '' ? new Date(val) : new Date(),
        this.dateFormatISO8601,
        this.dateLocale
      );
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = (e.target as any).value;
    this.onChangeFN(this.value);
  }

  onKeyUp(e: Event) {
    this.callOnChange(e);
    this.keyUp.emit(e);
  }
}

import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlValueAccessor,
  ControlContainer,
  FormGroup,
  FormControl,
  FormControlStatus,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: RadioButtonComponent,
    },
  ],
})
export class RadioButtonComponent implements ControlValueAccessor, OnInit {
  @Input() checked: boolean = false;
  @Input() public formControlName: string | null = '';
  value: any;
  @Input() radioValue: any;
  @Input() name: string = '';
  @Output() change = new EventEmitter();
  randomId: number = 0;

  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}
  ngOnInit(): void {
    this.randomId = Math.random();
  }

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null{
    if(!this.formControlName){
      return null
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if(!this.control){
      return 'PENDING'
    }
    return this.control.status;
  }

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  set touched(b: boolean) {}

  set form(fG: FormGroup) {}

  set control(fC: FormControl | null) {}

  set status(fS: FormControlStatus) {}

  writeValue(obj: string): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = (e.target as any).value;
    this.onChangeFN(this.value);
  }
}

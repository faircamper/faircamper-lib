import {
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  FormControlStatus,
  FormGroup,
  NG_VALUE_ACCESSOR,
  SelectControlValueAccessor,
} from '@angular/forms';
@Component({
  selector: 'fc-lib-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SelectComponent,
    },
  ],
})
export class SelectComponent implements ControlValueAccessor {
  @Input() placeholder = 'Placeholder';
  @Input() public formControlName: string | null = '';
  @Input() value: any = null;
  @Input() valueType: 'string' | 'number' = 'string';
  @Input() disabled = false;
  @Output() change = new EventEmitter();
  @Output() keyUp = new EventEmitter();
  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null {
    if(!this.formControlName){
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if(!this.control){
      return 'PENDING';
    }
    return this.control.status;
  }

  get touched(): boolean {
    if(!this.control){
      return false;
    }
    return this.control.touched;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = this.typeTransform((e.target as any).value);
    this.onChangeFN(this.value);
  }

  typeOfFn(value: any) {
    return typeof value;
  }

  typeTransform(value: string): string | number | object {
    switch (this.valueType) {
      case 'number':
        return +value;
      default:
        return value;
    }
  }
}

import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormControl,
  FormControlStatus,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-standalone-checkbox',
  templateUrl: './standalone-checkbox.component.html',
  styleUrls: ['./standalone-checkbox.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: StandAloneCheckbox,
    },
  ],
})
export class StandAloneCheckbox implements ControlValueAccessor, OnInit {
  @Input() checked = false;
  @Input() public formControlName: string | null = '';
  @Input() value: boolean | string = false;
  @ViewChild(Input) input!: HTMLElement;
  @Output() change = new EventEmitter();
  @Input() align: 'left' | 'center' | 'right' = 'left';
  @Input() alignMD: 'left' | 'center' | 'right' = 'left';
  @Input() alignLG: 'left' | 'center' | 'right' = 'left';
  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};
  randomId = '0';

  constructor(@Optional() private controlContainer: ControlContainer) {}

  ngOnInit(): void {
    this.randomId = Math.random().toString().replace('.', '');
    if (this.checked) {
      this.value = this.checked;
    }
  }

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  set form(fc: FormGroup) {}

  get control(): FormControl | null {
    if (!this.formControlName) {
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  set control(fc: FormControl | null) {}

  get status(): FormControlStatus {
    if (!this.control) {
      return 'PENDING';
    }
    return this.control.status;
  }

  set status(fC: FormControlStatus) {}

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  set touched(fc: boolean) {}

  writeValue(obj: boolean): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = (e.target as any).checked;
    this.onChangeFN(this.value);
  }
}

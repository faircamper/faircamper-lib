import { TestBed } from '@angular/core/testing';

import { FcLibService } from './fc-lib.service';

describe('FcLibService', () => {
  let service: FcLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FcLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

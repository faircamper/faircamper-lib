import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'fc-lib-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent implements OnInit {
  @Input() show = false;
  @Input() size: 'xl' | 'lg' | 'md' | 'sm' | 'xs' = 'xl';
  @Output() closed = new EventEmitter();
  @Output() opened = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  close() {
    this.show = false;
    this.closed.emit();
  }
}

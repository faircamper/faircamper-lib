import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputErrorComponent } from '../input-error/input-error.component';
import { InputHintComponent } from '../input-hint/input-hint.component';
import { LabelComponent } from '../label/label.component';
import { CheckboxComponent } from './checkbox.component';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;
  

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxComponent , LabelComponent, InputHintComponent, InputErrorComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have random id', () => {
    const randomId = component.randomId;
    expect(randomId).not.toBe('');
  });

  it('should check if the checked value have default - false', () => {    
    const checkboxInput = fixture.debugElement.nativeElement.querySelector('input');
    expect(checkboxInput.checked).toBeFalsy();
  });

  it('should check if the checkbox checked value can be changed after click', () => {    
    const checkboxInput = fixture.debugElement.nativeElement.querySelector('input');
    expect(checkboxInput.checked).toBeFalsy();
    checkboxInput.click()
    fixture.detectChanges();
    expect(checkboxInput.checked).toBeTruthy();
  });
});
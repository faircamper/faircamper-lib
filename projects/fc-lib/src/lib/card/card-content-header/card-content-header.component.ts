import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-card-content-header',
  templateUrl: './card-content-header.component.html',
  styleUrls: ['./card-content-header.component.css'],
})
export class CardContentHeaderComponent implements OnInit {
  @Input() color: 'orange-500' | 'standard' = 'standard';
  constructor() {}

  ngOnInit(): void {}
}

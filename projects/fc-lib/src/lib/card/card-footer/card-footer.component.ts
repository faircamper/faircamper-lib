import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-card-footer',
  templateUrl: './card-footer.component.html',
  styleUrls: ['./card-footer.component.css'],
})
export class CardFooterComponent implements OnInit {
  @Input() color: 'orange-500' | 'standard' = 'standard';
  constructor() {}

  ngOnInit(): void {}
}

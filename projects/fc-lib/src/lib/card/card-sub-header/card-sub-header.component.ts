import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-card-sub-header',
  templateUrl: './card-sub-header.component.html',
  styleUrls: ['./card-sub-header.component.css'],
})
export class CardSubHeaderComponent implements OnInit {
  @Input() color: 'orange-500' | 'standard' = 'standard';
  constructor() {}

  ngOnInit(): void {}
}

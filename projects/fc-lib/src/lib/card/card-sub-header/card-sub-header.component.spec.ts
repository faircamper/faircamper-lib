import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSubHeaderComponent } from './card-sub-header.component';

describe('CardSubHeaderComponent', () => {
  let component: CardSubHeaderComponent;
  let fixture: ComponentFixture<CardSubHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardSubHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSubHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

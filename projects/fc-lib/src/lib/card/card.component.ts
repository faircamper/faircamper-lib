import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() rounded = true;
  @Input() color: 'orange-500' | 'standard' = 'standard';
  constructor() {}

  ngOnInit(): void {}
}

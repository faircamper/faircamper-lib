import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-card-header',
  templateUrl: './card-header.component.html',
  styleUrls: ['./card-header.component.css'],
})
export class CardHeaderComponent implements OnInit {
  @Input() color: 'orange-500' | 'standard' = 'standard';
  constructor() {}

  ngOnInit(): void {}
}

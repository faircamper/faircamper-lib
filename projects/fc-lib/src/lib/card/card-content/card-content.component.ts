import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-card-content',
  templateUrl: './card-content.component.html',
  styleUrls: ['./card-content.component.css'],
})
export class CardContentComponent implements OnInit {
  @Input() rounded = true;
  @Input() color: 'orange-500' | 'standard' = 'standard';
  constructor() {}

  ngOnInit(): void {}
}

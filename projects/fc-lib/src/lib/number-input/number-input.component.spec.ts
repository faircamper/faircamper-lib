import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputErrorComponent } from '../input-error/input-error.component';
import { InputHintComponent } from '../input-hint/input-hint.component';
import { LabelComponent } from '../label/label.component';

import { NumberInputComponent } from './number-input.component';

describe('NumberInputComponent', () => {
  let component: NumberInputComponent;
  let fixture: ComponentFixture<NumberInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NumberInputComponent , LabelComponent, InputHintComponent, InputErrorComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

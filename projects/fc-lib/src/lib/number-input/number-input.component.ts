import {
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
} from '@angular/core';
import {
  ControlValueAccessor,
  ControlContainer,
  FormGroup,
  FormControl,
  FormControlStatus,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'fc-lib-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: NumberInputComponent,
    },
  ],
})
export class NumberInputComponent implements ControlValueAccessor {
  @Input() public formControlName: string | null = '';
  @Input() value: number = 0;
  @Input() min = '0';
  @Output() change = new EventEmitter();
  @Output() keyUp = new EventEmitter();

  onChangeFN = (_: any) => {};
  onTouchedFN = () => {};

  constructor(@Optional() private controlContainer: ControlContainer) {}

  get form(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  get control(): FormControl | null {
    if (!this.formControlName) {
      return null;
    }
    return this.form.get(this.formControlName) as FormControl;
  }

  get status(): FormControlStatus {
    if (!this.control) {
      return 'PENDING';
    }
    return this.control.status;
  }

  get touched(): boolean {
    if (!this.control) {
      return false;
    }
    return this.control.touched;
  }

  set form(v: FormGroup) {}

  set control(v: FormControl | null) {}

  set status(v: FormControlStatus) {}

  set touched(v: boolean) {}

  writeValue(obj: number): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeFN = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedFN = fn;
  }

  callOnChange(e: Event): void {
    this.value = +(e.target as any).value;
    this.onChangeFN(+this.value);
    
  }

  onKeyUp(e: Event) {
    this.callOnChange(e);
    this.keyUp.emit(e);
  }

  increase() {
    this.value++;
    this.onChangeFN(+this.value);
    this.onTouchedFN();
  }

  decrease() {
    this.value--;
    if(this.value < 0 ){
      this.value = 0;
    }
    this.onChangeFN(+this.value);
    this.onTouchedFN();
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { FormControlStatus } from '@angular/forms';

@Component({
  selector: 'fc-lib-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.css'],
})
export class InputErrorComponent implements OnInit {
  @Input() status: FormControlStatus = 'PENDING';
  @Input() touched = false;
  constructor() {}

  ngOnInit(): void {}
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FcLibComponent } from './fc-lib.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { ShowMoreSectionComponent } from './show-more-section/show-more-section.component';
import { VehicleTypeIconComponent } from './vehicle-type-icon/vehicle-type-icon.component';
import { CardComponent } from './card/card.component';
import { CardHeaderComponent } from './card/card-header/card-header.component';
import { CardContentComponent } from './card/card-content/card-content.component';
import { CardFooterComponent } from './card/card-footer/card-footer.component';
import { ExpansionPanelComponent } from './expansion-panel/expansion-panel.component';
import { AccordionComponent } from './accordion/accordion.component';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { DialogComponent } from './dialog/dialog.component';
import { ButtonComponent } from './button/button.component';
import { IconComponent } from './icon/icon.component';
import { TextInputComponent } from './text-input/text-input.component';
import { TextAreaComponent } from './text-area/text-area.component';
import { SelectComponent } from './select/select.component';
import { LabelComponent } from './label/label.component';
import { InputHintComponent } from './input-hint/input-hint.component';
import { InputErrorComponent } from './input-error/input-error.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { RadioButtonComponent } from './radio-button/radio-button.component';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';
import { NumberInputComponent } from './number-input/number-input.component';
import { TableComponent } from './table/table.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DateRangeComponent } from './date-range/date-range.component';
import { CardSubHeaderComponent } from './card/card-sub-header/card-sub-header.component';
import { CardContentHeaderComponent } from './card/card-content-header/card-content-header.component';
import { StandAloneCheckbox } from './standalone-checkbox/standalone-checkbox.component';
import { StandAloneNumberInputComponent } from './standalone-number-input/standalone-number-input.component';

@NgModule({
  declarations: [
    FcLibComponent,
    SpinnerComponent,
    ShowMoreSectionComponent,
    VehicleTypeIconComponent,
    CardComponent,
    CardHeaderComponent,
    CardContentComponent,
    CardFooterComponent,
    ExpansionPanelComponent,
    AccordionComponent,
    DialogComponent,
    ButtonComponent,
    IconComponent,
    TextInputComponent,
    TextAreaComponent,
    SelectComponent,
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
    CheckboxComponent,
    RadioButtonComponent,
    ToggleSwitchComponent,
    NumberInputComponent,
    TableComponent,
    DatepickerComponent,
    DateRangeComponent,
    CardSubHeaderComponent,
    CardContentHeaderComponent,
    StandAloneCheckbox,
    StandAloneNumberInputComponent,
  ],
  imports: [CommonModule, CdkAccordionModule, ReactiveFormsModule],
  exports: [
    FcLibComponent,
    SpinnerComponent,
    ShowMoreSectionComponent,
    VehicleTypeIconComponent,
    CardComponent,
    CardHeaderComponent,
    CardSubHeaderComponent,
    CardContentComponent,
    CardFooterComponent,
    ExpansionPanelComponent,
    AccordionComponent,
    DialogComponent,
    ButtonComponent,
    IconComponent,
    TextInputComponent,
    TextAreaComponent,
    SelectComponent,
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
    CheckboxComponent,
    NumberInputComponent,
    TableComponent,
    DatepickerComponent,
    ToggleSwitchComponent,
    RadioButtonComponent,
    CardContentHeaderComponent,
    StandAloneCheckbox,
    StandAloneNumberInputComponent,
  ],
})
export class FcLibModule {}

import {
  AfterContentInit,
  Component,
  ComponentFactoryResolver,
  ContentChildren,
  OnInit,
} from '@angular/core';
import { CdkAccordion } from '@angular/cdk/accordion';
import { ExpansionPanelComponent } from '../expansion-panel/expansion-panel.component';

@Component({
  selector: 'fc-lib-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.css'],
})
export class AccordionComponent
  extends CdkAccordion
  implements OnInit, AfterContentInit
{
  @ContentChildren(ExpansionPanelComponent)
  expansionPanels: ExpansionPanelComponent[] = [];
  lastTabOpened: number = -1;
  ngOnInit(): void {}

  ngAfterContentInit(): void {
    this.expansionPanels.forEach(
      (panel: ExpansionPanelComponent, currentIndex) => {
        panel.accordion = this;
        panel.insideAccordion = true;
        panel.tabindex = currentIndex;
        panel.opened.subscribe(() => {
          this.lastTabOpened = panel.tabindex;
          if (!this.multi) {
            this.closeAllOthers(panel.tabindex);
          }
        });
      }
    );
  }

  closeAllOthers(id: number) {
    this.expansionPanels.forEach(
      (panel: ExpansionPanelComponent, currentIndex) => {
        if (panel.tabindex !== id) {
          panel.close();
        }
      }
    );
  }

  openNext(tabIndex?: number) {
    if (!tabIndex) {
      tabIndex = this.lastTabOpened;
    }
    this.expansionPanels.forEach(
      (panel: ExpansionPanelComponent, currentIndex) => {
        if (panel.tabindex === tabIndex! + 1) {
          panel.open();
          this.closeAllOthers(panel.tabindex);
        }
      }
    );
  }

  openLast(tabIndex?: number) {
    if (!tabIndex) {
      tabIndex = this.lastTabOpened;
    }
    this.expansionPanels.forEach(
      (panel: ExpansionPanelComponent, currentIndex) => {
        if (panel.tabindex === tabIndex! - 1) {
          panel.open();
          this.closeAllOthers(panel.tabindex);
        }
      }
    );
  }
}

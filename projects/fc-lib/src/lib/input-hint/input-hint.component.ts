import { Component, Input, OnInit } from '@angular/core';
import { FormControlStatus } from '@angular/forms';

@Component({
  selector: 'fc-lib-input-hint',
  templateUrl: './input-hint.component.html',
  styleUrls: ['./input-hint.component.css'],
})
export class InputHintComponent implements OnInit {
  @Input() status: FormControlStatus = 'PENDING';
  @Input() touched = false;
  constructor() {}

  ngOnInit(): void {}
}

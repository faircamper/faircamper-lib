import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleTypeIconComponent } from './vehicle-type-icon.component';

describe('VehicleTypeIconComponent', () => {
  let component: VehicleTypeIconComponent;
  let fixture: ComponentFixture<VehicleTypeIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleTypeIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleTypeIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

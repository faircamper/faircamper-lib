import { Component, Input } from '@angular/core';
@Component({
  selector: 'fc-lib-vehicle-type-icon',
  templateUrl: './vehicle-type-icon.component.html',
  styleUrls: ['./vehicle-type-icon.component.css'],
})
export class VehicleTypeIconComponent {
  @Input() classString: string = 'h-10 mx-auto';
  @Input() id: number = 0;
  @Input() iconFormat: '' | 'thin' | 'bold' = '';
  
  constructor() {}
  ngOnInit(): void {
    if(this.iconFormat !== ''){this.classString = 'h-20 mx-auto'}
  }
}

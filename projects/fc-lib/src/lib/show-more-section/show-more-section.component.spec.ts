import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMoreSectionComponent } from './show-more-section.component';

describe('ShowMoreSectionComponent', () => {
  let component: ShowMoreSectionComponent;
  let fixture: ComponentFixture<ShowMoreSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowMoreSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMoreSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});

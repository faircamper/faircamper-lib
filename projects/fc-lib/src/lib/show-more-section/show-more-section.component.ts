import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fc-lib-show-more-section',
  templateUrl: './show-more-section.component.html',
  styleUrls: ['./show-more-section.component.css'],
})
export class ShowMoreSectionComponent implements OnInit {
  @Input() showMore = false;
  @Input() limit = 200;
  constructor() {}

  ngOnInit(): void {}
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputErrorComponent } from '../input-error/input-error.component';
import { InputHintComponent } from '../input-hint/input-hint.component';
import { LabelComponent } from '../label/label.component';

import { StandAloneNumberInputComponent } from './standalone-number-input.component';

describe('NumberInputComponent', () => {
  let component: StandAloneNumberInputComponent;
  let fixture: ComponentFixture<StandAloneNumberInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StandAloneNumberInputComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StandAloneNumberInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

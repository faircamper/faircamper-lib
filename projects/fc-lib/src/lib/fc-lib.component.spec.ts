import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FcLibComponent } from './fc-lib.component';

describe('FcLibComponent', () => {
  let component: FcLibComponent;
  let fixture: ComponentFixture<FcLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FcLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FcLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

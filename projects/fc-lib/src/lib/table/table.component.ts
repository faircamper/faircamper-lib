import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'fc-lib-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  @Input() sort = '';
  @Input() direction = '';
  @Input() content: any[] = [];
  contentKeys: string[] = [];
  @Input() columns: { name: string; sortable: boolean; sort: string }[] = [];
  @Input() actionsColumn: boolean = false;
  @Input() actionsString = 'Aktionen';
  @Input() endOfResults = false;
  @Output() sortingChanged = new EventEmitter();
  @Output() loadMore = new EventEmitter();
  constructor() {}

  ngOnInit(): void {
    if (this.content.length > 0) {
      this.contentKeys = this.content[0].keys;
    }
  }

  setSort(str: string, sortable: boolean) {
    if (this.sort === str && sortable) {
      this.direction = this.direction === 'asc' ? 'desc' : 'asc';
    } else {
      this.direction = 'asc';
      this.sort = str;
    }
    this.sortingChanged.emit({
      sort: this.sort,
      direction: this.direction,
      sortable: sortable,
    });
  }
}

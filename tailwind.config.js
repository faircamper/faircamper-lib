module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{html,ts}', './projects/fc-lib/src/**/*.{html,ts}'],
  theme: {
    screens: {
      sm: '375px',
      md: '768px',
      lg: '1200px',
    },
    colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'white': '#ffffff',
        'black': '#000000',
        'gray-100': '#F4F4F4',
        'gray-200': '#E0E0E0',
        'gray-400': '#999999',
        'gray-500': '#666666',
        'gray-700': '#535353',
        'gray-800': '#333333',
        'gray-900': '#1C1C1C',
        'orange-100': '#F9DABB',
        'orange-500': '#FF7F00',
        'blue-50': '#edf2f9',
        'blue-200': '#668DCD',
        'blue-300': '#5C83C3',
        'blue-400': '#4985D4',
        'blue-500': '#2C72C7',
        'blue-600': '#0546AD',
        'blue-900': '#002A57',
        'green-400': '#36CA54',
        'green-500': '#27b807',
        'error': '#FF3C0B',
        // custom by me
        'orange-light': '#ffb07b',
      },
    fontFamily: {
      sans: ['Fira Sans','Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    extend: {
      transitionProperty: {
        'height': 'height',
        'margin': 'margin',
      },
      fontSize: {
        'inputLabel': ['0.938rem', {'lineHeight': '1.625rem'}],
      }
    }
  },
  plugins: [require('@tailwindcss/forms'),require('@tailwindcss/typography')],
}

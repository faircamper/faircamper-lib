import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccordionTestComponent } from './example-projects/accordion-test/accordion-test.component';
import { ButtonTestComponent } from './example-projects/button-test/button-test.component';
import { CardTestComponent } from './example-projects/card-test/card-test.component';
import { CheckboxTestComponent } from './example-projects/checkbox-test/checkbox-test.component';
import { DatepickerTestComponent } from './example-projects/datepicker-test/datepicker-test.component';
import { NumberInputTestComponent } from './example-projects/number-input-test/number-input-test.component';
import { RadioButtonTestComponent } from './example-projects/radio-button-test/radio-button-test.component';
import { SelectTestComponent } from './example-projects/select-test/select-test.component';
import { ShowMoreSectionTestComponent } from './example-projects/show-more-section-test/show-more-section-test.component';
import { SpinnerTestComponent } from './example-projects/spinner-test/spinner-test.component';
import { TableTestComponent } from './example-projects/table-test/table-test.component';
import { TextAreaTestComponent } from './example-projects/text-area-test/text-area-test.component';
import { TextInputTestComponent } from './example-projects/text-input-test/text-input-test.component';
import { ToggleSwitchTestComponent } from './example-projects/toggle-switch-test/toggle-switch-test.component';

const routes: Routes = [
  {
  path:"text-input-test",
  component: TextInputTestComponent,
  },
  {
  path:"text-area-test",
  component: TextAreaTestComponent,
  },
  {
  path:"select-test",
  component: SelectTestComponent,
  },
  {
  path:"datepicker-test",
  component: DatepickerTestComponent,
  },
  {
  path:"number-input-test",
  component: NumberInputTestComponent,
  },
  {
  path:"checkbox-test",
  component: CheckboxTestComponent,
  },
  {
  path:"card-test",
  component: CardTestComponent,
  },
  {
  path:"button-test",
  component: ButtonTestComponent,
  },
  {
  path:"spinner-test",
  component: SpinnerTestComponent,
  },
  {
  path:"radio-button-test",
  component: RadioButtonTestComponent,
  },
  {
  path:"toggle-switch-test",
  component: ToggleSwitchTestComponent,
  },
  {
  path:"accordion-test",
  component: AccordionTestComponent,
  },
  {
  path:"table-test",
  component: TableTestComponent,
  },
  {
  path:"show-more-section-test",
  component: ShowMoreSectionTestComponent,
  },
];
  
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

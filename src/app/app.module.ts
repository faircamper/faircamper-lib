import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SelectTestComponent } from './example-projects/select-test/select-test.component';
import { TextInputTestComponent } from './example-projects/text-input-test/text-input-test.component';
import { DatepickerTestComponent } from './example-projects/datepicker-test/datepicker-test.component';
import { NumberInputTestComponent } from './example-projects/number-input-test/number-input-test.component';
import { CheckboxTestComponent } from './example-projects/checkbox-test/checkbox-test.component';
import { TextAreaTestComponent } from './example-projects/text-area-test/text-area-test.component';
import { CardTestComponent } from './example-projects/card-test/card-test.component';
import { ButtonTestComponent } from './example-projects/button-test/button-test.component';
import { SpinnerTestComponent } from './example-projects/spinner-test/spinner-test.component';
import { RadioButtonTestComponent } from './example-projects/radio-button-test/radio-button-test.component';
import { ToggleSwitchTestComponent } from './example-projects/toggle-switch-test/toggle-switch-test.component';
import { AccordionTestComponent } from './example-projects/accordion-test/accordion-test.component';
import { TableTestComponent } from './example-projects/table-test/table-test.component';
import { ShowMoreSectionTestComponent } from './example-projects/show-more-section-test/show-more-section-test.component';




@NgModule({
  declarations: [
    AppComponent,
    TextInputTestComponent,
    SelectTestComponent,
    DatepickerTestComponent,
    NumberInputTestComponent,
    CheckboxTestComponent,
    TextAreaTestComponent,
    CardTestComponent,
    ButtonTestComponent,
    SpinnerTestComponent,
    RadioButtonTestComponent,
    ToggleSwitchTestComponent,
    AccordionTestComponent,
    TableTestComponent,
    ShowMoreSectionTestComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FcLibModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

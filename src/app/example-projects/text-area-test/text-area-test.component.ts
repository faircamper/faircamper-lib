import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-text-area-test',
  templateUrl: './text-area-test.component.html',
  styleUrls: ['./text-area-test.component.css']
})
export class TextAreaTestComponent implements OnInit {

  form!: FormGroup;
  disabled = false;
  @Output() outputValue = new EventEmitter();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      inputControl: ['',[Validators.required]]
    })
  }

  get getFormGroup(): FormGroup {
    return this.form as FormGroup;
  }
  get getInputControl(): FormControl{   
    return this.getFormGroup.get('inputControl') as FormControl;
  }

  submit() {
    const inputValue = this.getInputControl.value;
    this.outputValue.emit(inputValue);
  }

}

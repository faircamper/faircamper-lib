import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { TextAreaTestComponent } from './text-area-test.component';

describe('TextAreaTestComponent', () => {
  let component: TextAreaTestComponent;
  let fixture: ComponentFixture<TextAreaTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextAreaTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAreaTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger the error-msg and hide the hint-msg', () => {
    const area = fixture.debugElement.query(By.css('fc-lib-text-area textarea'));
    area.triggerEventHandler('blur', {});
    fixture.detectChanges();
    const newErrorElement = fixture.debugElement.query(By.css('fc-lib-input-error div')).nativeNode.classList;
    const newHintElement = fixture.debugElement.query(By.css('fc-lib-input-hint div')).nativeNode.classList;
    expect(newHintElement).toContain('opacity-0')
    expect(newErrorElement).toContain('opacity-100');
  });

  it('should set disabled', () => {
    const area = fixture.debugElement.query(By.css('fc-lib-text-area textarea'))
    area.nativeElement.disabled = true;    
    fixture.detectChanges();
    const newElement = fixture.debugElement.query(By.css('fc-lib-text-area textarea'))
    expect(newElement.nativeElement.disabled).toBeTruthy();
  });
  
});

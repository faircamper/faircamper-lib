import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-test',
  templateUrl: './button-test.component.html',
  styleUrls: ['./button-test.component.css']
})
export class ButtonTestComponent implements OnInit {

  color = 'basic';
  fill = false;
  disabled = false;

  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { ButtonTestComponent } from './button-test.component';

describe('ButtonTestComponent', () => {
  let component: ButtonTestComponent;
  let fixture: ComponentFixture<ButtonTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set disabled', () => {
    const button = fixture.debugElement.query(By.css('fc-lib-button'))
    button.nativeElement.disabled = true;    
    fixture.detectChanges();
    const newElement = fixture.debugElement.query(By.css('fc-lib-button'))
    expect(newElement.nativeElement.disabled).toBeTruthy();
  });

  it('should set color', () => {
    const buttonClassList = fixture.debugElement.query(By.css('fc-lib-button button')).nativeNode.classList;
    expect(buttonClassList).toContain('border-blue-900');
    expect(buttonClassList).toContain('text-blue-900');
    component.color = 'danger'; 
    fixture.detectChanges();
    const newButtonClassList = fixture.debugElement.query(By.css('fc-lib-button button')).nativeNode.classList;
    expect(newButtonClassList).toContain('border-red');
    expect(newButtonClassList).toContain('text-red');
  });

  it('should set fill', () => {
    const buttonClassList = fixture.debugElement.query(By.css('fc-lib-button button')).nativeNode.classList;
    expect(buttonClassList).not.toContain('text-white');
    component.fill = true; 
    fixture.detectChanges();
    const newButtonClassList = fixture.debugElement.query(By.css('fc-lib-button button')).nativeNode.classList;
    expect(newButtonClassList).toContain('bg-blue-900');
    expect(newButtonClassList).toContain('text-white');
  });
});

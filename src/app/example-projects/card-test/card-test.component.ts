import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-test',
  templateUrl: './card-test.component.html',
  styleUrls: ['./card-test.component.css'],
})
export class CardTestComponent implements OnInit {
  @Input() rounded = true;
  @Input() color: 'orange-500' | 'standard' = 'orange-500';
  constructor() {}

  ngOnInit(): void {}
}

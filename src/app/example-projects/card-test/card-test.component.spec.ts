import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { CardTestComponent } from './card-test.component';

describe('CardTestComponent', () => {
  let component: CardTestComponent;
  let fixture: ComponentFixture<CardTestComponent>;
  let cardTexts = {
    header: 'Header here',
    content: 'Content for the card test',
    footer: 'Footer here',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CardTestComponent],
      imports: [FormsModule, ReactiveFormsModule, FcLibModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change is not rounded', () => {
    component.rounded = false;
    fixture.detectChanges();
    expect(fixture.componentInstance.rounded).toBeFalsy();
  });

  it('should change is rounded', () => {
    component.rounded = true;
    fixture.detectChanges();
    expect(fixture.componentInstance.rounded).toBeTruthy();
  });

  it('should set color', () => {
    const card = fixture.debugElement.query(By.css('fc-lib-card div'))
      .nativeNode.classList;
    expect(card).toContain('border-blue-100');
    component.color = 'orange-500';
    fixture.detectChanges();
    expect(card).toContain('border-orange-500');
  });

  it('should check header', () => {
    const header = fixture.debugElement.query(By.css('fc-lib-card-header div'))
      .nativeElement as HTMLElement;
    expect(header.textContent).toContain(cardTexts.header);
  });

  it('should check content', () => {
    const content = fixture.debugElement.query(By.css('fc-lib-card-content'))
      .nativeElement as HTMLElement;
    expect(content.textContent).toContain(cardTexts.content);
  });

  it('should check footer', () => {
    const footer = fixture.debugElement.query(By.css('fc-lib-card-footer'))
      .nativeElement as HTMLElement;
    expect(footer.textContent).toContain(cardTexts.footer);
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';

import { SelectTestComponent } from './select-test.component';

describe('SelectTestComponent', () => {
  let component: SelectTestComponent;
  let fixture: ComponentFixture<SelectTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectTestComponent , LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check exists options', () => {
    const selectOptions = fixture.nativeElement.querySelector('select').options;
    const innerText = 'test-option-';
    for (let i = 0; i < selectOptions.length; i++) {
      const element = selectOptions[i];
      if(i === 0){
        expect(element.textContent).toContain('-- Wähle eine Option');
      }else{
        expect(element.textContent).toContain(innerText+i);
      }
    }
  });

  it('should select a new option', () => {
    const select = fixture.nativeElement.querySelector('select');
    const indexToSelect = 3;
    select.value = select.options[indexToSelect].value;
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const selectElement = fixture.nativeElement.querySelector('select')
    selectElement.click();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('select').options.selectedIndex).toEqual(indexToSelect);
  });

});

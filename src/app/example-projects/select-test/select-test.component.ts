import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-select-test',
  templateUrl: './select-test.component.html',
  styleUrls: ['./select-test.component.css']
})
export class SelectTestComponent implements OnInit {

  
  form!: FormGroup;
  @Output() outputValue = new EventEmitter();

  placeholder = "Wähle eine Option";
  disabled = false;
  allSelects = [
    { id: 0, name: 'test-option-1' },
    { id: 1, name: 'test-option-2' },
    { id: 2, name: 'test-option-3' },
    { id: 3, name: 'test-option-4' },
    { id: 4, name: 'test-option-5' },
    { id: 5, name: 'test-option-6' },
    { id: 6, name: 'test-option-7' },
  ];
  
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      selectControl: [null]
    })
  }

  get getSelectControl(): FormControl{   
    return this.form.get('selectControl') as FormControl;
  }

  onChangeOption(currentSelect: number): void {
    if (!currentSelect) {
      return;
    }
    this.submit(currentSelect)
  }

  submit(currentSelect: number): void {
    console.log("test: ",currentSelect);
  }
}
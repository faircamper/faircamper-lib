import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-datepicker-test',
  templateUrl: './datepicker-test.component.html',
  styleUrls: ['./datepicker-test.component.css']
})
export class DatepickerTestComponent implements OnInit {

  @Input() public formControlName: string | null = '';
  @Input() dateFormatISO8601 = 'yyyy-MM-dd';
  @Input() dateLocale: 'de_DE' | 'en_EN' = 'en_EN';
  disabled = false;
  @Input() value: string = '';
  @Input() min: string = '';
  @Input() max: string = '';

  @Output() change = new EventEmitter();
  @Output() keyUp = new EventEmitter();


  form!: FormGroup;
  
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      datepickerControl: ['', [Validators.required]]
    })
  }

  get getFormGroup(): FormGroup {
    return this.form as FormGroup;
  }

  get getDatepickerControl(): FormControl{   
    return this.getFormGroup.get('datepickerControl') as FormControl;
  }
  
  callOnChange(e: Event): void {
    this.value = (e.target as any).value;
  }

  onKeyUp(e: Event) {
    this.callOnChange(e);
    this.keyUp.emit(e);
  }
}

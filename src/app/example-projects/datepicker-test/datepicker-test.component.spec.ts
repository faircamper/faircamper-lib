import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { FcLibModule } from "fc-lib";
import { DatepickerTestComponent } from "./datepicker-test.component";


describe('DatepickerComponent', () => {
  let component: DatepickerTestComponent;
  let fixture: ComponentFixture<DatepickerTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatepickerTestComponent],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set disabled', () => {
    const datepickerInput = fixture.debugElement.query(By.css('fc-lib-datepicker input'))
    datepickerInput.nativeElement.disabled = true;    
    fixture.detectChanges();
    const newElement = fixture.debugElement.query(By.css('fc-lib-datepicker input'))
    expect(newElement.nativeElement.disabled).toBeTruthy();
  });

  it('should trigger the error-msg and hide the hint-msg', () => {
    const datepicker = fixture.debugElement.query(By.css('fc-lib-datepicker div input'));
    datepicker.triggerEventHandler('blur', {});
    fixture.detectChanges();
    const newErrorElement = fixture.debugElement.query(By.css('fc-lib-input-error div')).nativeNode.classList;
    const newHintElement = fixture.debugElement.query(By.css('fc-lib-input-hint div')).nativeNode.classList;
    expect(newHintElement).toContain('opacity-0')
    expect(newErrorElement).toContain('opacity-100');
  });

  it('should emit on change', () => {
    const spyChange = spyOn(component.change, 'emit')
    const testDate = '2022-01-23';
    component.change.emit(testDate)
    fixture.detectChanges();
    expect(spyChange).toHaveBeenCalledWith(testDate);
  });

  it('should emit on keyUp', () => {
    const spyKeyUp = spyOn(component.keyUp, 'emit')
    const testDate = '2021-01-23';
    component.keyUp.emit(testDate)
    fixture.detectChanges();
    expect(spyKeyUp).toHaveBeenCalledWith(testDate);
  });

  it('should set min and max date', () => {
    component.min = '2021-01-01';
    component.max = '2022-12-31';
    component.value = '2022-04-23'
    fixture.detectChanges();
    const newDatePicker = fixture.debugElement.nativeElement as HTMLElement
    expect(newDatePicker.querySelector('input')?.value).not.toEqual('');
    expect(new Date(component.max).getTime()).toBeGreaterThan(new Date(component.min).getTime());
  });

});

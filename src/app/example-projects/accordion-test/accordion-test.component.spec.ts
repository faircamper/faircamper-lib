import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { AccordionTestComponent } from './accordion-test.component';

describe('AccordionTestComponent', () => {
  let component: AccordionTestComponent;
  let fixture: ComponentFixture<AccordionTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccordionTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain component id', () => {
    const componentId = fixture.debugElement.query(By.css('fc-lib-accordion')).componentInstance.id;
    expect(componentId).toContain('cdk-accordion-');
  });

  it('should check to open panel', () => {
    const button = fixture.debugElement.queryAll(By.css('fc-lib-button'));
    button[5].nativeElement.click();
    button[5].nativeElement.click();
    button[5].nativeElement.click();
    fixture.detectChanges();
    const newPanels = getPanels();
    expect(newPanels[2].query(By.css('div.overflow-hidden.transition-height')).styles['height']).toBe('133px');
    expect(newPanels[1].query(By.css('div.overflow-hidden.transition-height')).styles['height'] 
            && newPanels[0].query(By.css('div.overflow-hidden.transition-height')).styles['height']).toBe('0px');

    button[4].nativeElement.click();
    fixture.detectChanges();
    const newPanels2 = getPanels();
    expect(newPanels[1].query(By.css('div.overflow-hidden.transition-height')).styles['height']).toBe('133px');
    expect(newPanels[2].query(By.css('div.overflow-hidden.transition-height')).styles['height'] 
            && newPanels[0].query(By.css('div.overflow-hidden.transition-height')).styles['height']).toBe('0px');
  });

  it('should check all headers and contents', () => {
    const panels = getPanels();
    for (let i = 0; i < panels.length; i++) {
      expect(panels[i].nativeNode.innerText).toContain('Header '+(i+=1))
      expect(panels[i].nativeNode.innerText).toContain('Test-Content in panel '+(i+=1))
    }
  });

  function getPanels(){
    fixture.detectChanges();
    const accordion = fixture.debugElement.queryAll(By.css('fc-lib-expansion-panel'));
    let panelStorage: any = [];
    for (let i = 0; i < accordion.length; i++) {
      const element = accordion[i];
      panelStorage.push(element);
    }
    return panelStorage;
  }
});

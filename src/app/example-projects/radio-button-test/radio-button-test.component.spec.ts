import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { RadioButtonTestComponent } from './radio-button-test.component';

describe('RadioButtonTestComponent', () => {
  let component: RadioButtonTestComponent;
  let fixture: ComponentFixture<RadioButtonTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadioButtonTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioButtonTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get right form value after click', () => {
    checkRadioButtons(0,1,false);
  });

  it('should check is input checked after click', () => {
    checkRadioButtons(1,0,false);
  });

  it('should get right form value after click', () => {
    checkRadioButtons(0,1,true);
  });

  it('should check is input checked after change selection', () => {
    checkRadioButtons(1,0,true);
  });  

  function checkRadioButtons(first:number,second:number,checkForm:boolean){
    const radioButtons = fixture.debugElement.queryAll(By.css('fc-lib-radio-button input'));
    let radioStorage: any = [];
    
    for (let i = 0; i < radioButtons.length; i++) {
      const element = radioButtons[i];
      radioStorage.push(element);
    }

    radioStorage[first].nativeElement.click();
    fixture.detectChanges();

    if(!checkForm){
      expect(radioButtons[first].nativeNode.checked).toBeTruthy();
      expect(radioButtons[second].nativeNode.checked).toBeFalsy();
    }else{
      expect(radioButtons[first].componentInstance.value).toBe(radioButtons[first].componentInstance.value);
      expect(radioButtons[second].componentInstance.value).toBe('');     
    }
  }

});

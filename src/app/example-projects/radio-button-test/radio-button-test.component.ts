import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-radio-button-test',
  templateUrl: './radio-button-test.component.html',
  styleUrls: ['./radio-button-test.component.css']
})
export class RadioButtonTestComponent implements OnInit {

  form!: FormGroup;
  checked = 'female';
  value:any;
  @Output() change = new EventEmitter();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      radioControl: ['',[]]
    })
  }

  get getFormGroup(): FormGroup {
    return this.form as FormGroup;
  }

  get getControl(): FormControl | null{
    return this.getFormGroup.get('radioControl') as FormControl;
  }

}

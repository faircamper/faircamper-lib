import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner-test',
  templateUrl: './spinner-test.component.html',
  styleUrls: ['./spinner-test.component.css']
})
export class SpinnerTestComponent implements OnInit {

  color: 'black' | 'white' | 'gray' | 'slate' = 'black'
  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { SpinnerTestComponent } from './spinner-test.component';

describe('SpinnerTestComponent', () => {
  let component: SpinnerTestComponent;
  let fixture: ComponentFixture<SpinnerTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpinnerTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set color', () => {
    const spinnerClassList = fixture.debugElement.query(By.css('fc-lib-spinner div')).nativeNode.classList;
    expect(spinnerClassList).toContain('text-black');
    component.color = 'slate'; 
    fixture.detectChanges();
    const newSpinnerClassList = fixture.debugElement.query(By.css('fc-lib-spinner div')).nativeNode.classList;
    expect(newSpinnerClassList).toContain('text-slate-500');
  });
});

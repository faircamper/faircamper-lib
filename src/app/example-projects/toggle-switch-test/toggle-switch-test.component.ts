import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-toggle-switch-test',
  templateUrl: './toggle-switch-test.component.html',
  styleUrls: ['./toggle-switch-test.component.css']
})
export class ToggleSwitchTestComponent implements OnInit {

  form!: FormGroup;
  disabled = false;
  value: any;
  @Output() change = new EventEmitter();
  
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      switchControl: ['',[]]
    })
  }

  get getFormGroup(): FormGroup {
    return this.form as FormGroup;
  }
  get getSwitchControl(): FormControl{   
    return this.getFormGroup.get('switchControl') as FormControl;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { ToggleSwitchTestComponent } from './toggle-switch-test.component';

describe('ToggleSwitchTestComponent', () => {
  let component: ToggleSwitchTestComponent;
  let fixture: ComponentFixture<ToggleSwitchTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToggleSwitchTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleSwitchTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set disabled', () => {
    const input = fixture.debugElement.query(By.css('fc-lib-toggle-switch'))
    input.nativeElement.disabled = true;    
    fixture.detectChanges();
    const newElement = fixture.debugElement.query(By.css('fc-lib-toggle-switch'))
    expect(newElement.nativeElement.disabled).toBeTruthy();
  });

  it('should check the toggle changed value after click', () => {    
    const toggleElement = fixture.debugElement.query(By.css('fc-lib-toggle-switch input'));
    toggleElement.nativeElement.click();
    fixture.detectChanges();  
    expect(toggleElement.componentInstance.value).toBeTruthy()
    toggleElement.nativeElement.click();  
    fixture.detectChanges();  
    expect(toggleElement.componentInstance.value).toBeFalsy()
  });
});

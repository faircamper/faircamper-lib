import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-checkbox-test',
  templateUrl: './checkbox-test.component.html',
  styleUrls: ['./checkbox-test.component.css']
})
export class CheckboxTestComponent implements OnInit {

  form!: FormGroup;
  disabled = false;
  @Output() outputValue = new EventEmitter();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      checkboxControl: ['',[Validators.required]]
    })
  }

  get getFormGroup(): FormGroup {
    return this.form as FormGroup;
  }
  get getNumberControl(): FormControl{   
    return this.getFormGroup.get('checkboxControl') as FormControl;
  }

  submit() {
    const inputValue = this.getNumberControl.value;
    console.log("Input-Value: ",inputValue);
    this.outputValue.emit(inputValue);
  }

}

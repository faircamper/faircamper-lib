import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FcLibModule } from 'fc-lib';

import { CheckboxTestComponent } from './checkbox-test.component';

describe('CheckboxTestComponent', () => {
  let component: CheckboxTestComponent;
  let fixture: ComponentFixture<CheckboxTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have random id', () => {
    const randomId = fixture.debugElement.nativeElement.querySelector('fc-lib-checkbox input').id;
    expect(randomId).not.toBe('');
  });

  it('should check the checked value has default - false', () => {    
    const checkboxInputElement = fixture.debugElement.nativeElement.querySelector('fc-lib-checkbox input');
    expect(checkboxInputElement.checked).toBeFalsy();
  });

  it('should check the checkbox checked value can be changed after click', () => {    
    const checkboxInputElement = fixture.debugElement.nativeElement.querySelector('fc-lib-checkbox input');
    expect(checkboxInputElement.checked).toBeFalsy();
    checkboxInputElement.click()
    fixture.detectChanges();
    expect(checkboxInputElement.checked).toBeTruthy();
  });
});

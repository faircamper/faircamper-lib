import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';
import { TextInputTestComponent } from './text-input-test.component';

describe('TextInputTestComponent', () => {
  let component: TextInputTestComponent;
  let fixture: ComponentFixture<TextInputTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextInputTestComponent],
      imports: [FormsModule, ReactiveFormsModule, FcLibModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextInputTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger the error-msg and hide the hint-msg', () => {
    const input = fixture.debugElement.query(By.css('fc-lib-text-input input'));
    input.triggerEventHandler('blur', {});
    fixture.detectChanges();
    const newErrorElement = fixture.debugElement.query(
      By.css('fc-lib-input-error div')
    ).nativeNode.classList;
    const newHintElement = fixture.debugElement.query(
      By.css('fc-lib-input-hint div')
    ).nativeNode.classList;
    expect(newHintElement).toContain('opacity-0');
    expect(newErrorElement).toContain('opacity-100');
  });

  it('should set disabled', () => {
    const input = fixture.debugElement.query(By.css('fc-lib-text-input input'));
    input.nativeElement.disabled = true;
    fixture.detectChanges();
    expect(input.nativeElement.disabled).toBeTruthy();
  });

  it('should set color', () => {
    const input = fixture.debugElement.query(By.css('fc-lib-text-input input'))
      .nativeNode.classList;
    expect(input).toContain('border-gray-400');
    component.color = 'orange-500';
    fixture.detectChanges();
    expect(input).toContain('border-orange-500');
  });
});

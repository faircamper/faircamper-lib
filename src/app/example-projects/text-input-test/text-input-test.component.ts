import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
@Component({
  selector: 'app-text-input-test',
  templateUrl: './text-input-test.component.html',
  styleUrls: ['./text-input-test.component.css'],
})
export class TextInputTestComponent implements OnInit {
  inputForm!: FormGroup;
  disabled = false;
  color: 'orange-500' | 'standard' = 'standard';
  @Output() outputValue = new EventEmitter();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.inputForm = this.fb.group({
      inputControl: ['', [Validators.required]],
    });
  }

  get getFormGroup(): FormGroup {
    return this.inputForm as FormGroup;
  }
  get getInputControl(): FormControl {
    return this.getFormGroup.get('inputControl') as FormControl;
  }

  submit() {
    const inputValue = this.getInputControl.value;
    this.outputValue.emit(inputValue);
  }
}

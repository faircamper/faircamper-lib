import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FcLibModule } from 'fc-lib';

import { NumberInputTestComponent } from './number-input-test.component';

describe('NumberInputTestComponent', () => {
  let component: NumberInputTestComponent;
  let fixture: ComponentFixture<NumberInputTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NumberInputTestComponent ],
      imports: [ FormsModule,              
        ReactiveFormsModule,
        FcLibModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberInputTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger the error-msg and hide the hint-msg', () => {
    const numberInput = fixture.debugElement.query(By.css('fc-lib-number-input input'));
    numberInput.triggerEventHandler('blur', {});
    fixture.detectChanges();
    const newErrorElement = fixture.debugElement.query(By.css('fc-lib-input-error div')).nativeNode.classList;
    const newHintElement = fixture.debugElement.query(By.css('fc-lib-input-hint div')).nativeNode.classList;
    expect(newHintElement).toContain('opacity-0')
    expect(newErrorElement).toContain('opacity-100');
  });

  it('should set disabled', () => {
    const numberInput = fixture.debugElement.query(By.css('fc-lib-number-input input'))
    numberInput.nativeElement.disabled = true;    
    fixture.detectChanges();
    const newElement = fixture.debugElement.query(By.css('fc-lib-number-input input'))
    expect(newElement.nativeElement.disabled).toBeTruthy();
  });

});

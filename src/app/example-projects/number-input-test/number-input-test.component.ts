import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-number-input-test',
  templateUrl: './number-input-test.component.html',
  styleUrls: ['./number-input-test.component.css']
})
export class NumberInputTestComponent implements OnInit {

  form!: FormGroup;
  disabled = false;
  @Output() outputValue = new EventEmitter();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      numberControl: ['',[Validators.required]]
    })
  }

  get getFormGroup(): FormGroup {
    return this.form as FormGroup;
  }
  get getNumberControl(): FormControl{   
    return this.getFormGroup.get('numberControl') as FormControl;
  }

  submit() {
    const inputValue = this.getNumberControl.value;
    console.log("Input-Value: ",inputValue);
    this.outputValue.emit(inputValue);
  }
}

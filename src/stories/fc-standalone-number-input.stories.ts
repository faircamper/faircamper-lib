import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { NumberInputComponent } from 'projects/fc-lib/src/lib/number-input/number-input.component';
import { StandAloneNumberInputComponent } from 'projects/fc-lib/src/lib/standalone-number-input/standalone-number-input.component';

export default {
  title: 'Inputs/Standalone Number',
  component: StandAloneNumberInputComponent,
  subcomponents: {},
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    value: {
      control: { type: 'number' },
    },
    align: {
      control: { type: 'select' },
      options: ['left', 'center', 'right'],
    },
    alignMD: {
      control: { type: 'select' },
      options: ['left', 'center', 'right'],
    },
    alignLG: {
      control: { type: 'select' },
      options: ['left', 'center', 'right'],
    },
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
    decrease: {
      table: {
        disable: true,
      },
    },
    increase: {
      table: {
        disable: true,
      },
    },
    onKeyUp: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<StandAloneNumberInputComponent> = (
  args: StandAloneNumberInputComponent
) => {
  let formGroup = new FormGroup({
    number: new FormControl(0),
  });
  return {
    props: { ...args, form: formGroup },
    template: `
      <form [formGroup]="form">
        <fc-lib-standalone-number-input formControlName="number" [value]="${args.value}" [align]="'${args.align}'" [alignMD]="'${args.alignMD}'" [alignLG]="'${args.alignLG}'">
        </fc-lib-standalone-number-input>
      </form>
    `,
  };
};

export const Basic = Template.bind({});
Basic.args = {};

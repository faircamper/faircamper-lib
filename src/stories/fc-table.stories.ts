import { Story, Meta } from '@storybook/angular/types-6-0';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { TableComponent } from 'projects/fc-lib/src/lib/table/table.component';
import { IconComponent } from 'projects/fc-lib/src/lib/icon/icon.component';
import { ButtonComponent } from 'projects/fc-lib/src/lib/button/button.component';

export default {
  title: 'Basics/Table',
  component: TableComponent,
  subcomponents: {},
  decorators: [
    moduleMetadata({
      declarations: [IconComponent, ButtonComponent],
      imports: [],
    }),
  ],
  argTypes: {
    rounded: {
      control: { type: 'radio' },
      defaultValue: false,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
    endOfResults: {
      control: { type: 'boolean' },
      defaultValue: false,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
  },
} as Meta;

const Template: Story<TableComponent> = (args: TableComponent) => {
  return {
    props: args,
    template: `
      <fc-lib-table
        [columns]="[
        { name: 'Fahrzeug', sortable: true, sort: 'vehicleId' },
        { name: 'Kennzeichen', sortable: true, sort: 'externalId' },
        { name: 'Status', sortable: false, sort: 'status' },
        ]"
        [sort]="'vehicleId'"
        [endOfResults]="${args.endOfResults}"
        [direction]="'asc'">
        <div class="table-row even:bg-gray-100 odd:bg-gray-200" content>
          <div class="table-cell px-5 py-1">Fahrzeug 1</div>
          <div class="table-cell px-5 py-1">HH-Karl-54</div>
          <div class="table-cell px-5 py-1">Online</div>
          <div class="table-cell px-5 py-1">Bearbeiten</div>
        </div>
        <div class="table-row even:bg-gray-100 odd:bg-gray-200" content>
          <div class="table-cell px-5 py-1">Fahrzeug 2</div>
          <div class="table-cell px-5 py-1">HH-Karl-54</div>
          <div class="table-cell px-5 py-1">Online</div>
          <div class="table-cell px-5 py-1">Bearbeiten</div>
        </div>
        <div class="table-row even:bg-gray-100 odd:bg-gray-200" content>
          <div class="table-cell px-5 py-1">Fahrzeug 3</div>
          <div class="table-cell px-5 py-1">HH-Karl-54</div>
          <div class="table-cell px-5 py-1">Online</div>
          <div class="table-cell px-5 py-1">Bearbeiten</div>
        </div>
        <div class="table-row even:bg-gray-100 odd:bg-gray-200" content>
          <div class="table-cell px-5 py-1">Fahrzeug 4</div>
          <div class="table-cell px-5 py-1">HH-Karl-54</div>
          <div class="table-cell px-5 py-1">Online</div>
          <div class="table-cell px-5 py-1">Bearbeiten</div>
        </div>
        <div class="table-row even:bg-gray-100 odd:bg-gray-200" content>
          <div class="table-cell px-5 py-1">Fahrzeug 5</div>
          <div class="table-cell px-5 py-1">HH-Karl-54</div>
          <div class="table-cell px-5 py-1">Online</div>
          <div class="table-cell px-5 py-1">Bearbeiten</div>
        </div>
      </fc-lib-table>
      `,
  };
};

export const Default = Template.bind({});
Default.args = {};

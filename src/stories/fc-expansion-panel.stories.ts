import { Story, Meta } from '@storybook/angular/types-6-0';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { ExpansionPanelComponent } from 'projects/fc-lib/src/lib/expansion-panel/expansion-panel.component';

export default {
  title: 'Special Use/Expansion Panel',
  component: ExpansionPanelComponent,
  subcomponents: {},
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [],
    }),
    componentWrapperDecorator(ExpansionPanelComponent),
  ],
  argTypes: {
    rounded: {
      control: { type: 'radio' },
      defaultValue: false,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
  },
} as Meta;

const Template: Story<ExpansionPanelComponent> = (
  args: ExpansionPanelComponent
) => ({
  props: args,
  template: `
  <p title>Header</p>
  <p discription>Discription</p>
  <p content>
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
    magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
    gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
    elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
    eos et
    accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
    sit
    amet.
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
    magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
    gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
    elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
    eos et
    accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
    sit
    amet.
  </p>
  <p footer>
    Awesome Footer
  </p>
  `,
});

export const Default = Template.bind({});
Default.args = { rounded: false };

import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { DateRangeComponent } from 'projects/fc-lib/src/lib/date-range/date-range.component';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';

export default {
  title: 'Inputs/Date Range',
  component: DateRangeComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    value: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<DateRangeComponent> = (args: DateRangeComponent) => {
  let formGroup = new FormGroup({
    check1: new FormControl(),
  });

  return {
    props: { ...args, form: formGroup },
    template: `
    <form [formGroup]="form">
      <fc-lib-date-range formControlName="check1">
        <span label>Label</span>
        <span hint> Hint </span>
        <span error> Error </span>
      </fc-lib-date-range>
    </form>
  `,
  };
};

export const Basic = Template.bind({});
Basic.args = {};

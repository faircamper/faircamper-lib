import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';
import { RadioButtonComponent } from 'projects/fc-lib/src/lib/radio-button/radio-button.component';

export default {
  title: 'Inputs/ Radio Button',
  component: RadioButtonComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    radioValue: {
      control: { type: 'select' },
      options: ['male', 'female'],
    },
    value: {
      table: {
        disable: true,
      },
    },
    name: {
      control: { type: 'text' },
      defaultValue: 'test',
    },
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<RadioButtonComponent> = (args: RadioButtonComponent) => {
  let formGroup = new FormGroup({
    radio1: new FormControl([args.radioValue]),
  });

  return {
    props: { ...args, form: formGroup },
    template: `
      <form [formGroup]="form">
        <fc-lib-radio-button [radioValue]="'male'" formControlName="radio1" [name]="'${args.name}'" [checked]="'${args.value}' === 'male'">
          <span label>Male</span>
          <span hint> Hint </span>
          <span error> Error </span>
        </fc-lib-radio-button>
        <fc-lib-radio-button [radioValue]="'female'"  formControlName="radio1" [name]="'${args.name}'" [checked]="'${args.value}' === 'female'">
          <span label>Female</span>
          <span hint> Hint </span>
          <span error> Error </span>
        </fc-lib-radio-button>
      </form>
      <div>
      </div>
    `,
  };
};

export const Basic = Template.bind({});
Basic.args = { name: 'test', value: 'female' };

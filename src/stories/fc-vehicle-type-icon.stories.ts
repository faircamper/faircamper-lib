import { Story, Meta } from '@storybook/angular/types-6-0';
import { VehicleTypeIconComponent } from 'projects/fc-lib/src/lib/vehicle-type-icon/vehicle-type-icon.component';

export default {
  title: 'Basics/Vehicle Type Icon',
  component: VehicleTypeIconComponent,
  argTypes: {
    id: {
      control: { type: 'select' },
      defaultValue: '',
      table: {
        category: 'Appearance',
      },
      options: [
        0,
        1,
        2,
        3,
        4,
        5,
      ],
    },
    iconFormat: {
      control: { type: 'select' },
      defaultValue: '',
      table: {
        category: 'Appearance',
      },
      options: [
      
        '',
        'thin',
        'bold',
      ],
    },
  },
} as Meta;

const Template: Story<VehicleTypeIconComponent> = (
  args: VehicleTypeIconComponent
) => ({
  props: args,
  template: `
    <div class="grid grid-cols-6">
      <p class="my-auto">Alkoven</p>
      <fc-lib-vehicle-type-icon [iconFormat]="''" [id]="0">
      </fc-lib-vehicle-type-icon>
      
      <p class="my-auto">Teilintegriert</p>
      <fc-lib-vehicle-type-icon [iconFormat]="''" [id]="1">
      </fc-lib-vehicle-type-icon>
      
      <p class="my-auto">Campingbus</p>
      <fc-lib-vehicle-type-icon [iconFormat]="''" [id]="2">
      </fc-lib-vehicle-type-icon>
      
      <p class="my-auto">Kastenwagen</p>
      <fc-lib-vehicle-type-icon [iconFormat]="''" [id]="3">
      </fc-lib-vehicle-type-icon>
      
      <p class="my-auto">Vollintegriert</p>
      <fc-lib-vehicle-type-icon [iconFormat]="''" [id]="4">
      </fc-lib-vehicle-type-icon>
      
      
      <p class="my-auto">Trailer</p>
      <fc-lib-vehicle-type-icon [iconFormat]="''" [id]="5">
      </fc-lib-vehicle-type-icon>
    
    `,
});

export const Empty = Template.bind({});
Empty.args = {
  id: 0, iconFormat: '',
};

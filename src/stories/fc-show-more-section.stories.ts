import { Story, Meta } from '@storybook/angular/types-6-0';
import { ShowMoreSectionComponent } from 'projects/fc-lib/src/lib/show-more-section/show-more-section.component';
import { SpinnerComponent } from 'projects/fc-lib/src/lib/spinner/spinner.component';

export default {
  title: 'special Use/Show More Section',
  component: ShowMoreSectionComponent,
  argTypes: {
    limit: {
      control: { type: 'number' },
      defaultValue: 200,
      table: {
        category: 'Size',
      },
    },
    showMore: {
      control: { type: 'radio' },
      defaultValue: true,
      options: [true, false],
    },
  },
} as Meta;

const Template: Story<ShowMoreSectionComponent> = (
  args: ShowMoreSectionComponent
) => ({
  props: args,
});

export const Default = Template.bind({});
Default.args = { showMore: true };

import { Story, Meta } from '@storybook/angular/types-6-0';
import { moduleMetadata } from '@storybook/angular';
import { DialogComponent } from 'projects/fc-lib/src/lib/dialog/dialog.component';

export default {
  title: 'Basics/Dialog',
  component: DialogComponent,
  subcomponents: {},
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [],
    }),
  ],
  argTypes: {
    size: {
      control: { type: 'select' },
      options: ['xl', 'lg', 'md', 'sm', 'xs'],
    },
    ngOnInit: {
      table: { disable: true },
    },
  },
} as Meta;

const Template: Story<DialogComponent> = (args: DialogComponent) => {
  return {
    props: args,
    template: `
    Test Text
    <br>To show the backdrop blur effect when opening the modal
    <br> Open the modal by setting the show argument to true <br> In the Menu below <br> Lorem Ipsum <br> Lorem Ipsum <br> Lorem Ipsum <br> Lorem Ipsum <br> Lorem Ipsum <br> Lorem Ipsum
    <br> Lorem Ipsum <br> Lorem Ipsum<br> Lorem Ipsum<br> Lorem Ipsum<br> Lorem Ipsum<br> Lorem Ipsum<br> Lorem Ipsum<br> Lorem Ipsum <br> Lorem Ipsum
    <!-- Button trigger modal -->
    <fc-lib-dialog [show]="${args.show}" [size]="'${args.size}'">
      <ng-container title>Lolo</ng-container>
      <ng-container content> <p >
          With less than a month to go before the European Union enacts new consumer privacy laws for its citizens,
          companies around the world are updating their terms of service agreements to comply.
        </p>
        <p >
          The European Union’s General Data Protection Regulation (G.D.P.R.) goes into effect on May 25 and is meant to
          ensure a common set of data rights in the European Union. It requires organizations to notify users as soon as
          possible of high-risk data breaches that could personally affect them.
        </p>
      </ng-container>
      <ng-container footer>
        <button data-modal-toggle="defaultModal" type="button"
          class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">Decline</button>
        <button data-modal-toggle="defaultModal" type="button"
          class="text-white bg-blue-200 hover:bg-blue-900 focus:ring-4 focus:outline-none focus:ring-blue-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-900 dark:focus:ring-9800">I
          accept</button>
        </ng-container>
    </fc-lib-dialog>
  `,
  };
};

export const XL = Template.bind({});
XL.args = { show: true, size: 'xl' };

export const LG = Template.bind({});
LG.args = { show: true, size: 'lg' };

export const MD = Template.bind({});
MD.args = { show: true, size: 'md' };

export const SM = Template.bind({});
SM.args = { show: true, size: 'sm' };

export const XS = Template.bind({});
XS.args = { show: true, size: 'xs' };

import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';
import { SelectComponent } from 'projects/fc-lib/src/lib/select/select.component';

export default {
  title: 'Inputs/Select',
  component: SelectComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    value: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<SelectComponent> = (args: SelectComponent) => {
  let formGroup = new FormGroup({
    select: new FormControl(),
  });
  const options = ['Banana', 'Apple', 'Pineapple'];
  return {
    props: { ...args, form: formGroup, options: options },
    template: `
    <form [formGroup]="form">
      <fc-lib-select formControlName="select" [placeholder]="'${args.placeholder}'">
        <span label>Label</span>
        <span hint> Hint </span>
        <span error> Error </span>
        <option *ngFor="let option of options" [value]="option">{{option}}</option>
      </fc-lib-select>
    </form>
    `,
  };
};

export const Basic = Template.bind({});
Basic.args = { placeholder: 'Select a Fruit' };

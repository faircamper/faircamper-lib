import { Story, Meta } from '@storybook/angular/types-6-0';
import { ButtonComponent } from 'projects/fc-lib/src/lib/button/button.component';
export default {
  title: 'Basics/Button',
  component: ButtonComponent,
  argTypes: {
    color: {
      control: { type: 'select' },
      defaultValue: 'primary',
      table: {
        category: 'Appearance',
      },
      options: ['basic', 'success', 'danger', 'primary', 'secondary'],
    },
    textColor: {
      control: { type: 'select' },
      defaultValue: 'default',
      table: {
        category: 'Appearance',
      },
      options: ['default', 'white'],
    },
    text: {
      control: { type: 'text' },
      table: {
        category: 'Content',
      },
    },
    borderColor: {
      control: { type: 'select' },
      table: {
        category: 'Appearance',
      },
      options: ['basic'],
    },
    border: {
      control: { type: 'radio' },
      defaultValue: false,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
    disabled: {
      control: { type: 'radio' },
      defaultValue: false,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
  },
} as Meta;

const Template: Story<ButtonComponent> = (args: ButtonComponent) => {
  let textInput = 'Einloggen';
  return {
    props: { ...args, text: textInput },
    template: `
      <fc-lib-button [color]="'${args.color}'" [fill]="${args.fill}" >
        <span>${textInput}</span>
      </fc-lib-button>
    `,
  };
};

export const Basic = Template.bind({});
Basic.args = {
  color: 'basic',
  fill: false,
};
export const BasicFill = Template.bind({});
BasicFill.args = { color: 'basic', fill: true };

export const Success = Template.bind({});
Success.args = { color: 'success', fill: true };

export const Danger = Template.bind({});
Danger.args = { color: 'danger', fill: true };

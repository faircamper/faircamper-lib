import { Story, Meta } from '@storybook/angular/types-6-0';
import { moduleMetadata } from '@storybook/angular';
import { CardContentComponent } from 'projects/fc-lib/src/lib/card/card-content/card-content.component';
import { CardFooterComponent } from 'projects/fc-lib/src/lib/card/card-footer/card-footer.component';
import { CardHeaderComponent } from 'projects/fc-lib/src/lib/card/card-header/card-header.component';
import { CardComponent } from 'projects/fc-lib/src/lib/card/card.component';
import { CardSubHeaderComponent } from 'projects/fc-lib/src/lib/card/card-sub-header/card-sub-header.component';
import { CardContentHeaderComponent } from 'projects/fc-lib/src/lib/card/card-content-header/card-content-header.component';

export default {
  title: 'Basics/Card',
  component: CardComponent,
  subcomponents: {
    CardContentComponent,
    CardHeaderComponent,
    CardSubHeaderComponent,
    CardFooterComponent,
    CardContentHeaderComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [
        CardContentComponent,
        CardHeaderComponent,
        CardSubHeaderComponent,
        CardFooterComponent,
        CardContentHeaderComponent,
      ],
      imports: [],
    }),
  ],
  argTypes: {
    rounded: {
      control: { type: 'radio' },
      defaultValue: true,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
    color: {
      control: { type: 'select' },
      defaultValue: 'standard',
      options: ['orange-500', 'standard'],
      table: {
        category: 'inputs',
      },
    },
  },
} as Meta;

const Template: Story<CardComponent> = (args: CardComponent) => ({
  props: args,
  template: `
    <fc-lib-card [rounded]="'${args.rounded}'" [color]="'${args.color}'">
      <fc-lib-card-header [color]="'${args.color}'">Header</fc-lib-card-header>
      <fc-lib-card-sub-header [color]="'${args.color}'">Sub Header</fc-lib-card-sub-header>
      <fc-lib-card-content>
      <fc-lib-card-content-header [color]="'${args.color}'">Content Header</fc-lib-card-content-header>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
      magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
      gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
      elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
      accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
      amet.</fc-lib-card-content>
      <fc-lib-card-footer>Footer</fc-lib-card-footer>
    </fc-lib-card>
  `,
});

export const Default = Template.bind({});
Default.args = {};

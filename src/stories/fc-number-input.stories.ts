import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';
import { NumberInputComponent } from 'projects/fc-lib/src/lib/number-input/number-input.component';

export default {
  title: 'Inputs/Number',
  component: NumberInputComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    value: {
      control: { type: 'number' },
    },
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
    decrease: {
      table: {
        disable: true,
      },
    },
    increase: {
      table: {
        disable: true,
      },
    },
    onKeyUp: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<NumberInputComponent> = (args: NumberInputComponent) => {
  let formGroup = new FormGroup({
    number: new FormControl(0),
  });
  return {
    props: { ...args, form: formGroup },
    template: `
      <form [formGroup]="form">
        <fc-lib-number-input formControlName="number" [value]="${args.value}">
        <span label>Label</span>
          <span hint> Hint </span>
          <span error> Error </span>
        </fc-lib-number-input>
      </form>
    `,
  };
};

export const Basic = Template.bind({});
Basic.args = {};

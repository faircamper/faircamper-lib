import { Story, Meta } from '@storybook/angular/types-6-0';
import { IconComponent } from 'projects/fc-lib/src/lib/icon/icon.component';
export default {
  title: 'Basics/Icon',
  component: IconComponent,
  argTypes: {
    icon: {
      control: { type: 'select' },
      defaultValue: '',
      table: {
        category: 'Appearance',
      },
      options: [
        '',
        'dashboard',
        'bookings',
        'booking-requests',
        'rental-request',
        'messages',
        'payments',
        'reciepts',
        'vehicles',
        'new-vehicle',
        'vehicle-classes',
        'locations',
        'seasons',
        'contracts',
        'account',
        'settings',
        'menu',
        'logout',
        'undo',
        'redo',
        'switch_horizontal',
        'switch_vertical',
        'reset',
        'refresh',
        'close',
        'upload',
        'edit',
        'add',
        'chevron-down',
        'chevron-up',
        'sort-asc',
        'sort-desc',
        'equipment',
        'check-action',
      ],
    },
  },
} as Meta;

const Template: Story<IconComponent> = (args: IconComponent) => {
  return {
    props: args,
    template: `
    <div class="grid grid-cols-8">
      dashboard
      <fc-lib-icon [icon]="'dashboard'" >
      </fc-lib-icon>
      bookings
      <fc-lib-icon [icon]="'bookings'" >
      </fc-lib-icon>
      booking-requests
      <fc-lib-icon [icon]="'booking-requests'" >
      </fc-lib-icon>
      rental-request
      <fc-lib-icon [icon]="'rental-request'" >
      </fc-lib-icon>
      messages
      <fc-lib-icon [icon]="'messages'" >
      </fc-lib-icon>
      payments
      <fc-lib-icon [icon]="'payments'" >
      </fc-lib-icon>
      reciepts
      <fc-lib-icon [icon]="'reciepts'" >
      </fc-lib-icon>
      vehicles
      <fc-lib-icon [icon]="'vehicles'" >
      </fc-lib-icon>
      new-vehicle
      <fc-lib-icon [icon]="'new-vehicle'" >
      </fc-lib-icon>
      vehicle-classes
      <fc-lib-icon [icon]="'vehicle-classes'" >
      </fc-lib-icon>
      locations
      <fc-lib-icon [icon]="'locations'" >
      </fc-lib-icon>
      seasons
      <fc-lib-icon [icon]="'seasons'" >
      </fc-lib-icon>
      contracts
      <fc-lib-icon [icon]="'contracts'" >
      </fc-lib-icon>
      account
      <fc-lib-icon [icon]="'account'" >
      </fc-lib-icon>
      settings
      <fc-lib-icon [icon]="'settings'" >
      </fc-lib-icon>
      menu
      <fc-lib-icon [icon]="'menu'" >
      </fc-lib-icon>
      undo
      <fc-lib-icon [icon]="'undo'" >
      </fc-lib-icon>
      redo
      <fc-lib-icon [icon]="'redo'" >
      </fc-lib-icon>
      switch_horizontal
      <fc-lib-icon [icon]="'switch_horizontal'" >
      </fc-lib-icon>
      switch_vertical
      <fc-lib-icon [icon]="'switch_vertical'" >
      </fc-lib-icon>
      reset
      <fc-lib-icon [icon]="'reset'" >
      </fc-lib-icon>
      refresh
      <fc-lib-icon [icon]="'refresh'" >
      </fc-lib-icon>
      close
      <fc-lib-icon [icon]="'close'" >
      </fc-lib-icon>
      upload
      <fc-lib-icon [icon]="'upload'" >
      </fc-lib-icon>
      edit
      <fc-lib-icon [icon]="'edit'" >
      </fc-lib-icon>
      add
      <fc-lib-icon [icon]="'add'" >
      </fc-lib-icon>
      chevron-down
      <fc-lib-icon [icon]="'chevron-down'" >
      </fc-lib-icon>
      chevron-up
      <fc-lib-icon [icon]="'chevron-up'" >
      </fc-lib-icon>
      sort-asc
      <fc-lib-icon [icon]="'sort-asc'" >
      </fc-lib-icon>
      sort-desc
      <fc-lib-icon [icon]="'sort-desc'" >
      </fc-lib-icon>
      equipment
      <fc-lib-icon [icon]="'equipment'" >
      </fc-lib-icon>
      check-bulletpoint
      <fc-lib-icon [icon]="'check-bulletpoint'" >
      </fc-lib-icon>
      check-action
      <fc-lib-icon [icon]="'check-action'" >
      </fc-lib-icon>
      error-msg
      <fc-lib-icon [icon]="'error-msg'" >
      </fc-lib-icon>
      warning-hint
      <fc-lib-icon [icon]="'warning-hint'" >
      </fc-lib-icon>
      delete
      <fc-lib-icon [icon]="'delete'" >
      </fc-lib-icon>
      avatar-user
      <fc-lib-icon [icon]="'avatar-user'" >
      </fc-lib-icon>
      calendar
      <fc-lib-icon [icon]="'calendar'" >
      </fc-lib-icon>
      pickup-return
      <fc-lib-icon [icon]="'pickup-return'" [iconFormat]="''">
      </fc-lib-icon>
      offer
      <fc-lib-icon [icon]="'offer'" [iconFormat]="''">
      </fc-lib-icon>
      num-of-persons
      <fc-lib-icon [icon]="'num-of-persons'" [iconFormat]="''">
      </fc-lib-icon>
      num-of-beds
      <fc-lib-icon [icon]="'num-of-beds'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x2
      <fc-lib-icon [icon]="'beds-x2'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x3
      <fc-lib-icon [icon]="'beds-x3'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x4
      <fc-lib-icon [icon]="'beds-x4'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x5
      <fc-lib-icon [icon]="'beds-x5'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x6
      <fc-lib-icon [icon]="'beds-x6'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x7
      <fc-lib-icon [icon]="'beds-x7'" [iconFormat]="''">
      </fc-lib-icon>
      beds-x8
      <fc-lib-icon [icon]="'beds-x8'" [iconFormat]="''">
      </fc-lib-icon>
      parental-leave
      <fc-lib-icon [icon]="'parental-leave'" [iconFormat]="''">
      </fc-lib-icon>
      early-booking
      <fc-lib-icon [icon]="'early-booking'" [iconFormat]="''">
      </fc-lib-icon>
      commercial-supplier
      <fc-lib-icon [icon]="'commercial-supplier'" [iconFormat]="''">
      </fc-lib-icon>
      weight-over-3.5t
      <fc-lib-icon [icon]="'weight-over-3.5t'" [iconFormat]="''">
      </fc-lib-icon>
      weight-over-4.25t
      <fc-lib-icon [icon]="'weight-over-4.25t'" [iconFormat]="''">
      </fc-lib-icon>
      weight-over-7.5t
      <fc-lib-icon [icon]="'weight-over-7.5t'" [iconFormat]="''">
      </fc-lib-icon>
      weight-under-3.5t
      <fc-lib-icon [icon]="'weight-under-3.5t'" [iconFormat]="''">
      </fc-lib-icon>
      weight-under-4.25t
      <fc-lib-icon [icon]="'weight-under-4.25t'" [iconFormat]="''">
      </fc-lib-icon>
      weight-under-7.5t
      <fc-lib-icon [icon]="'weight-under-7.5t'" [iconFormat]="''">
      </fc-lib-icon>
      larger-than
      <fc-lib-icon [icon]="'larger-than'" [iconFormat]="''">
      </fc-lib-icon>
      voucher
      <fc-lib-icon [icon]="'voucher'" [iconFormat]="''">
      </fc-lib-icon>
      info
      <fc-lib-icon [icon]="'info'" [iconFormat]="''">
      </fc-lib-icon>
      interested-parties
      <fc-lib-icon [icon]="'interested-parties'" [iconFormat]="''">
      </fc-lib-icon>
      less-than
      <fc-lib-icon [icon]="'less-than'" [iconFormat]="''">
      </fc-lib-icon>
      long-term-rental
      <fc-lib-icon [icon]="'long-term-rental'" [iconFormat]="''">
      </fc-lib-icon>
      max-80-kmh
      <fc-lib-icon [icon]="'max-80-kmh'" [iconFormat]="''">
      </fc-lib-icon>
      max-90-kmh
      <fc-lib-icon [icon]="'max-90-kmh'" [iconFormat]="''">
      </fc-lib-icon>
      max-100-kmh
      <fc-lib-icon [icon]="'max-100-kmh'" [iconFormat]="''">
      </fc-lib-icon>
      max-110-kmh
      <fc-lib-icon [icon]="'max-110-kmh'" [iconFormat]="''">
      </fc-lib-icon>
      max-120-kmh
      <fc-lib-icon [icon]="'max-120-kmh'" [iconFormat]="''">
      </fc-lib-icon>
      nights
      <fc-lib-icon [icon]="'nights'" [iconFormat]="''">
      </fc-lib-icon>
      private-supplier
      <fc-lib-icon [icon]="'private-supplier'" [iconFormat]="''">
      </fc-lib-icon>
      destinations   
      <fc-lib-icon [icon]="'destinations'" [iconFormat]="''">
      </fc-lib-icon>
      seats-2x
      <fc-lib-icon [icon]="'seats-2x'" [iconFormat]="''">
      </fc-lib-icon>
      seats-3x
      <fc-lib-icon [icon]="'seats-3x'" [iconFormat]="''">
      </fc-lib-icon>
      seats-4x
      <fc-lib-icon [icon]="'seats-4x'" [iconFormat]="''">
      </fc-lib-icon>
      seats-5x
      <fc-lib-icon [icon]="'seats-5x'" [iconFormat]="''">
      </fc-lib-icon>
      seats-6x
      <fc-lib-icon [icon]="'seats-6x'" [iconFormat]="''">
      </fc-lib-icon>
      seats-7x
      <fc-lib-icon [icon]="'seats-7x'" [iconFormat]="''">
      </fc-lib-icon>
      seats-8x
      <fc-lib-icon [icon]="'seats-8x'" [iconFormat]="''">
      </fc-lib-icon>
      location
      <fc-lib-icon [icon]="'location'" [iconFormat]="''">
      </fc-lib-icon>
      animals
      <fc-lib-icon [icon]="'animals'" [iconFormat]="''">
      </fc-lib-icon>
      animals-allowed
      <fc-lib-icon [icon]="'animals-allowed'" [iconFormat]="''">
      </fc-lib-icon>
      animals-prohibited
      <fc-lib-icon [icon]="'animals-prohibited'" [iconFormat]="''">
      </fc-lib-icon>
      non-binding-request
      <fc-lib-icon [icon]="'non-binding-request'" [iconFormat]="''">
      </fc-lib-icon>
    </div>
    `,
  };
};

export const Empty = Template.bind({});
Empty.args = {
  icon: '',
};

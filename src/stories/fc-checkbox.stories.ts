import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { CheckboxComponent } from 'projects/fc-lib/src/lib/checkbox/checkbox.component';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';

export default {
  title: 'Inputs/Checkbox',
  component: CheckboxComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    value: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<CheckboxComponent> = (args: CheckboxComponent) => {
  let formGroup = new FormGroup({
    check1: new FormControl(true),
  });

  return {
    props: { ...args, form: formGroup },
    template: `
    <form [formGroup]="form">
      <fc-lib-checkbox formControlName="check1" [checked]="${args.checked}">
        <span label>Label</span>
        <span hint> Hint </span>
        <span error> Error </span>
      </fc-lib-checkbox>
    </form>
  `,
  };
};

export const Basic = Template.bind({});
Basic.args = { checked: true };

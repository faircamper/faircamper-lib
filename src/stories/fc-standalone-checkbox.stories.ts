import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { StandAloneCheckbox } from 'projects/fc-lib/src/lib/standalone-checkbox/standalone-checkbox.component';

export default {
  title: 'Inputs/Standalone Checkbox',
  component: StandAloneCheckbox,
  subcomponents: {},
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    align: {
      control: { type: 'select' },
      options: ['left', 'center', 'right'],
    },
    alignMD: {
      control: { type: 'select' },
      options: ['left', 'center', 'right'],
    },
    alignLG: {
      control: { type: 'select' },
      options: ['left', 'center', 'right'],
    },
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    value: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<StandAloneCheckbox> = (args: StandAloneCheckbox) => {
  let formGroup = new FormGroup({
    check1: new FormControl(true),
  });

  return {
    props: { ...args, form: formGroup },
    template: `
    <form [formGroup]="form">
      <fc-lib-standalone-checkbox formControlName="check1" [checked]="${args.checked}" [align]="'${args.align}'" [alignMD]="'${args.alignMD}'" [alignLG]="'${args.alignLG}'">
      </fc-lib-standalone-checkbox>
    </form>
  `,
  };
};

export const Basic = Template.bind({});
Basic.args = { checked: true };

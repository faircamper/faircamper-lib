import { formatDate } from '@angular/common';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { DatepickerComponent } from 'projects/fc-lib/src/lib/datepicker/datepicker.component';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';

export default {
    title: 'Inputs/Datepicker',
    component: DatepickerComponent,
    subcomponents: {
      LabelComponent,
      InputHintComponent,
      InputErrorComponent,
    },
    decorators: [
      moduleMetadata({
        declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
        imports: [ReactiveFormsModule],
      }),
    ],
    argTypes: {
      status: {
        table: {
          disable: true,
        },
      },
      formControlName: {
        table: {
          disable: true,
        },
      },
      value: {
        control: { type: 'date' },
        defaultValue:formatDate(new Date(), 'YYYY-MM-dd', 'en_US'),
        table: {
          disable: true,
        },
      },
      min: {
        control: { type: 'date' },
        defaultValue:formatDate(new Date(), 'YYYY-MM-dd', 'en_US'),
        table: {
            category: 'Input',
            disable: false,
        },
      },
      max: {
        control: { type: 'date' },
        defaultValue:formatDate(new Date('2022-05-31'), 'YYYY-MM-dd', 'en_US'),
        table: {
            category: 'Input',
            disable: false,
        },
      },
      touched: {
        table: {
          disable: true,
        },
      },
      randomId: {
        table: {
          disable: true,
        },
      },
      onChangeFN: {
        table: {
          disable: true,
        },
      },
      onTouchedFN: {
        table: {
          disable: true,
        },
      },
      callOnChange: {
        table: {
          disable: true,
        },
      },
      ngOnInit: {
        table: {
          disable: true,
        },
      },
      registerOnChange: {
        table: {
          disable: true,
        },
      },
      registerOnTouched: {
        table: {
          disable: true,
        },
      },
      writeValue: {
        table: {
          disable: true,
        },
      },
      input: {
        table: {
          disable: true,
        },
      },
      disabled: {
        control: { type: 'radio' },
        defaultValue:false,
        table: {
          category: 'Appearance',
          disabled: false,
        },
        options: [true, false],
      },
    },
  } as Meta;
  
  const Template: Story<DatepickerComponent> = (args: DatepickerComponent) => {
    
    let formGroup = new FormGroup({
      datepicker: new FormControl(),
    });
    return {
      props: { ...args, form: formGroup },
      template: `
      <form [formGroup]="form">
        <fc-lib-datepicker formControlName="datepicker" min="${formatDate(new Date(args.min), 'YYYY-MM-dd', 'en_US')}" max="${formatDate(new Date(args.max), 'YYYY-MM-dd', 'en_US')}" value="${formatDate(new Date(args.value), 'YYYY-MM-dd', 'en_US')}" [disabled]="${args.disabled}">
          <span label>Label</span>
          <span hint> hint </span>
          <span error> Error </span>
        </fc-lib-datepicker>
      </form>
      `,
    };
  };
  
  export const Basic = Template.bind({});
  Basic.args = { };
  
  
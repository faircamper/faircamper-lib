import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';
import { TextInputComponent } from 'projects/fc-lib/src/lib/text-input/text-input.component';

export default {
  title: 'Inputs/Text',
  component: TextInputComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    value: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
    onKeyUp: {
      table: {
        disable: true,
      },
    },
    color: {
      control: { type: 'select' },
      defaultValue: 'standard',
      options: ['standard', 'orange-500'],
      table: {
        category: 'inputs',
      },
    },
  },
} as Meta;

const Template: Story<TextInputComponent> = (args: TextInputComponent) => {
  let formGroup = new FormGroup({
    textControl: new FormControl(),
  });
  return {
    props: { ...args, form: formGroup },
    template: `
    <form [formGroup]="form">
      <fc-lib-text-input formControlName="textControl" [placeholder]="'${args.placeholder}'" [color]="'${args.color}' ">
        <span label>Label</span>
        <span hint> Hint </span>
        <span error> Error </span>
      </fc-lib-text-input>
    </form>
  `,
  };
};

export const Basic = Template.bind({});
Basic.args = {};

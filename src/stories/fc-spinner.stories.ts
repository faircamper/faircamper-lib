import { Story, Meta } from '@storybook/angular/types-6-0';
import { SpinnerComponent } from 'projects/fc-lib/src/lib/spinner/spinner.component';

export default {
  title: 'Basics/Spinner',
  component: SpinnerComponent,
  argTypes: {
    color: {
      control: { type: 'select' },
      defaultValue: 'black',
      options: ['black', 'white', 'gray', 'slate'],
      table: {
        category: 'Color',
      },
    },
    showText: {
      control: { type: 'radio' },
      defaultValue: true,
      options: [true, false],
      table: {
        category: 'Text',
      },
    },
  },
} as Meta;

const Template: Story<SpinnerComponent> = (args: SpinnerComponent) => ({
  props: args,
});

export const Default = Template.bind({});
Default.args = { color: 'black' };

export const White = Template.bind({});
White.args = { color: 'white' };

export const Gray = Template.bind({});
Gray.args = { color: 'gray' };

export const Slate = Template.bind({});
Slate.args = { color: 'slate' };

import { Story, Meta } from '@storybook/angular/types-6-0';
import { moduleMetadata } from '@storybook/angular';
import { AccordionComponent } from 'projects/fc-lib/src/lib/accordion/accordion.component';
import { ExpansionPanelComponent } from 'projects/fc-lib/src/lib/expansion-panel/expansion-panel.component';

export default {
  title: 'Special Use/Accordion',
  component: AccordionComponent,
  subcomponents: {
    ExpansionPanelComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [ExpansionPanelComponent],
      imports: [],
    }),
  ],
  argTypes: {
    multi: {
      control: { type: 'radio' },
      defaultValue: false,
      table: {
        category: 'Appearance',
      },
      options: [true, false],
    },
  },
} as Meta;

const Template: Story<AccordionComponent> = (args: AccordionComponent) => ({
  props: args,
  template: `
    <fc-lib-accordion [multi]="${args.multi}" #accordion>
      <fc-lib-expansion-panel #panel1>
        <div title>Header</div>
        <div discription>Discription</div>
        <div content>
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
          elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
          eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
          sit
          amet.
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
          elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
          eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
          sit
          amet.

        </div>
        <div footer>
          <button class=" bg-blue-200 p-2 rounded-lg" (click)="accordion.openNext(panel1.tabindex)">Next</button>
        </div>
      </fc-lib-expansion-panel>
      <fc-lib-expansion-panel #panel2>
        <div title>Header Longer</div>
        <div discription>Discription</div>
        <div content>
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
          elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
          eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
          sit
          amet.
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
          elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
          eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
          sit
          amet.
        </div>
        <div footer>
          <button class=" bg-red p-2 rounded-lg" (click)="accordion.openLast(panel2.tabindex)">Back</button>
          <button class=" bg-blue-200 p-2 rounded-lg" (click)="accordion.openNext(panel2.tabindex)">Next</button>
        </div>
      </fc-lib-expansion-panel>
       <fc-lib-expansion-panel #panel3>
        <div title>Header Longer</div>
        <div discription>Discription Longer</div>
        <div content>
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
          elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
          eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
          sit
          amet.
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
          gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
          elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
          eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
          sit
          amet.
        </div>
        <div footer>
          <button class=" bg-red p-2 rounded-lg" (click)="accordion.openLast(panel3.tabindex)">Back</button>
        </div>
      </fc-lib-expansion-panel>
    </fc-lib-accordion>
    <div class="h-5"></div>
    <button class=" bg-red p-2 rounded-lg" (click)="accordion.openLast()">Back</button>
    &nbsp;
    <button class=" bg-blue-200 p-2 rounded-lg" (click)="accordion.openNext()">Next</button>

    <div class="p-5 mt-5 rounded bg-orange-500">Note: This example may not completly work inside of storybook, regarding automatic close and open functionality (next & back) buttons </div>
  `,
});

export const Single = Template.bind({});
Single.args = { multi: false };

export const Multi = Template.bind({});
Multi.args = { multi: true };

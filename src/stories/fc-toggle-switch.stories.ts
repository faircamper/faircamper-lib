import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { InputErrorComponent } from 'projects/fc-lib/src/lib/input-error/input-error.component';
import { InputHintComponent } from 'projects/fc-lib/src/lib/input-hint/input-hint.component';
import { LabelComponent } from 'projects/fc-lib/src/lib/label/label.component';
import { ToggleSwitchComponent } from 'projects/fc-lib/src/lib/toggle-switch/toggle-switch.component';

export default {
  title: 'Inputs/Toggle Switch',
  component: ToggleSwitchComponent,
  subcomponents: {
    LabelComponent,
    InputHintComponent,
    InputErrorComponent,
  },
  decorators: [
    moduleMetadata({
      declarations: [LabelComponent, InputHintComponent, InputErrorComponent],
      imports: [ReactiveFormsModule],
    }),
  ],
  argTypes: {
    status: {
      table: {
        disable: true,
      },
    },
    formControlName: {
      table: {
        disable: true,
      },
    },
    value: {
      table: {
        disable: true,
      },
    },
    touched: {
      table: {
        disable: true,
      },
    },
    randomId: {
      table: {
        disable: true,
      },
    },
    onChangeFN: {
      table: {
        disable: true,
      },
    },
    onTouchedFN: {
      table: {
        disable: true,
      },
    },
    callOnChange: {
      table: {
        disable: true,
      },
    },
    ngOnInit: {
      table: {
        disable: true,
      },
    },
    registerOnChange: {
      table: {
        disable: true,
      },
    },
    registerOnTouched: {
      table: {
        disable: true,
      },
    },
    writeValue: {
      table: {
        disable: true,
      },
    },
    input: {
      table: {
        disable: true,
      },
    },
    onKeyUp: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<ToggleSwitchComponent> = (
  args: ToggleSwitchComponent
) => {
  let formGroup = new FormGroup({
    switchControl: new FormControl(),
  });
  return {
    props: { ...args, form: formGroup },
    template: `
    <form [formGroup]="form">
      <fc-lib-toggle-switch formControlName="switchControl">
        <span label>Label</span>
        <span hint> Hint </span>
        <span error> Error </span>
      </fc-lib-toggle-switch>
    </form>
    `,
  };
};

export const Basic = Template.bind({});
Basic.args = {};
